

// Галерея изображений

var worksButton = document.querySelectorAll('.works__menu-item');
var galleryItem = document.querySelectorAll('.works__gallery-item');
var galleryItemBranding = document.querySelectorAll('.works__gallery_branding');
var galleryItemDesign = document.querySelectorAll('.works__gallery_design');
var galleryItemDevelopment = document.querySelectorAll('.works__gallery_development');
var galleryItemStrategy = document.querySelectorAll('.works__gallery_strategy');


function changeBranding(){
    for(i=0;i<galleryItemDesign.length;i++) {
    galleryItemDesign[i].classList.add('unvisible');
    galleryItemDevelopment[i].classList.add('unvisible');
    galleryItemStrategy[i].classList.add('unvisible');
  }
  for(a=0;a<galleryItemBranding.length;a++){
  galleryItemBranding[a].classList.remove('unvisible'); 
  }
  for(b=0;b<worksButton.length;b++) {
    if(b==1) {
      worksButton[b].classList.add('works__menu-item_selected');
    } else {
      worksButton[b].classList.remove('works__menu-item_selected');
    }
  }
  }

  function changeDesign(){
    for(i=0;i<galleryItemDesign.length;i++) {
    galleryItemDesign[i].classList.remove('unvisible');
    galleryItemDevelopment[i].classList.add('unvisible');
    galleryItemStrategy[i].classList.add('unvisible');
  }
  for(a=0;a<galleryItemBranding.length;a++) {
    galleryItemBranding[a].classList.add('unvisible');
  }
  for(b=0;b<worksButton.length;b++) {
    if(b==2) {
      worksButton[b].classList.add('works__menu-item_selected');
    } else {
      worksButton[b].classList.remove('works__menu-item_selected');
    }
  }
  }

  function changeDevelopment(){
    for(i=0;i<galleryItemDevelopment.length;i++) {
    galleryItemDevelopment[i].classList.remove('unvisible');
    galleryItemDesign[i].classList.add('unvisible');
    galleryItemStrategy[i].classList.add('unvisible');
  }
  for(a=0;a<galleryItemBranding.length;a++) {
    galleryItemBranding[a].classList.add('unvisible');
  }
  for(b=0;b<worksButton.length;b++) {
    if(b==3) {
      worksButton[b].classList.add('works__menu-item_selected');
    } else {
      worksButton[b].classList.remove('works__menu-item_selected');
    }
  }
  }

  function changeStrategy(){
    for(i=0;i<galleryItemStrategy.length;i++) {
    galleryItemStrategy[i].classList.remove('unvisible');
    galleryItemDesign[i].classList.add('unvisible');
    galleryItemDevelopment[i].classList.add('unvisible');
  }
  for(a=0;a<galleryItemBranding.length;a++) {
    galleryItemBranding[a].classList.add('unvisible');
  }
  for(b=0;b<worksButton.length;b++) {
    if(b==4) {
      worksButton[b].classList.add('works__menu-item_selected');
    } else {
      worksButton[b].classList.remove('works__menu-item_selected');
    }
  }
  }

  function showAll(){
    for(i=0;i<galleryItemStrategy.length;i++) {
    galleryItemStrategy[i].classList.remove('unvisible');
    galleryItemDesign[i].classList.remove('unvisible');
    galleryItemDevelopment[i].classList.remove('unvisible');
  }
  for(a=0;a<galleryItemBranding.length;a++) {
    galleryItemBranding[a].classList.remove('unvisible');
  }
  for(b=0;b<worksButton.length;b++) {
    if(b==0) {
      worksButton[b].classList.add('works__menu-item_selected');
    } else {
      worksButton[b].classList.remove('works__menu-item_selected');
    }
  }
  }

  worksButton[0].addEventListener('click',showAll);
  worksButton[1].addEventListener('click',changeBranding);
  worksButton[2].addEventListener('click',changeDesign);
  worksButton[3].addEventListener('click',changeDevelopment);
  worksButton[4].addEventListener('click',changeStrategy);


  // Слайдер
  var slide = document.querySelectorAll('.about__slider-content');
  var navLeft = document.querySelector('.about__slider-left');
  var navRight = document.querySelector('.about__slider-right');
  var currentPos = 0;
  console.log(navRight);
  
  navRight.addEventListener('click', function(event) {
   currentPos=currentPos-25.6;
   if(currentPos<-110) {
     currentPos=0
   }
   slide[0].style.left=currentPos + '%';
  })

  navLeft.addEventListener('click', function(event) {
    currentPos=currentPos+25.6;
    if( currentPos > 0) {
      currentPos= -102;
    }
    slide[0].style.left=currentPos + '%';
   })

  

  //Прокрутка 


  function anim(duration) {

  var temp;
    return function(sel) {
    cancelAnimationFrame(temp);
    var start = performance.now();
    var from = window.pageYOffset || document.documentElement.scrollTop,
    to = document.querySelector(sel).getBoundingClientRect().top;
    requestAnimationFrame(function step(timestamp) {
    var progress = (timestamp - start) / duration;
    1 <= progress && (progress = 1);
    window.scrollTo(0, from + to * progress | 0);
    1 > progress && (temp = requestAnimationFrame(step))
    })
    }
  };
  var scrollMenu = anim(500)

  var upButton = document.querySelector('.scroll-top-button');

  function showButton() {
    var x = pageYOffset;
    if(x>250) {
      upButton.style.opacity = "100";
    } else {
      upButton.style.opacity = "0";
    }
  }
  window.addEventListener('scroll', showButton);

  // Слайдер 2 

  var clientSliderBut = document.querySelectorAll('.clients__control-unit');
  var clientSlider = document.querySelectorAll('.clients__item');
  // var left = 0;

  clientSliderBut[0].addEventListener('click', function(event) {
    
    clientSlider[0].classList.remove('showing');
    clientSlider[1].classList.add('showing');
    clientSlider[2].classList.add('showing');
    clientSliderBut[0].style.backgroundColor='#c0301c'
    clientSliderBut[1].style.backgroundColor='#dddddd'
    clientSliderBut[2].style.backgroundColor='#dddddd'

  })


  clientSliderBut[1].addEventListener('click', function(event) {
    clientSlider[0].classList.add('showing');
    clientSlider[1].classList.remove('showing');
    clientSlider[2].classList.add('showing');
    clientSliderBut[0].style.backgroundColor='#dddddd'
    clientSliderBut[1].style.backgroundColor='#c0301c'
    clientSliderBut[2].style.backgroundColor='#dddddd'

  })

  clientSliderBut[2].addEventListener('click', function(event) {
    clientSlider[0].classList.add('showing');
    clientSlider[1].classList.add('showing');
    clientSlider[2].classList.remove('showing');
    clientSliderBut[0].style.backgroundColor='#dddddd'
    clientSliderBut[1].style.backgroundColor='#dddddd'
    clientSliderBut[2].style.backgroundColor='#c0301c'

  })





  // Валидация формы
  
  var form = document.forms[0];
  var submitBtn = form.elements['submit-btn'];

  function disableBtn(){
    submitBtn.disabled = 'true' ;
  }
  form.addEventListener('submit',disableBtn) ;

  //Боковое меню

  var menuBtn = document.querySelector('.header__burger');
  var menu = document.querySelector('.header__menu_adaptive');

  menuBtn.addEventListener('click', function(event){
    menu.classList.toggle('hide_menu');
  })